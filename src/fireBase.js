import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";

// TODO: Replace the following with your app's Firebase project configuration
// See: https://firebase.google.com/docs/web/learn-more#config-object
const firebaseConfig = {
    apiKey: "AIzaSyCpDkG1vBUDilrSzZKMTYxrxj5O2d8Z6WQ",
    authDomain: "thuongvt123456-1fcdd.firebaseapp.com",
    projectId: "thuongvt123456-1fcdd",
    storageBucket: "thuongvt123456-1fcdd.appspot.com",
    messagingSenderId: "668373701860",
    appId: "1:668373701860:web:cebd7c75acc1a15abc08f1"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);


// Initialize Firebase Authentication and get a reference to the service
const auth = getAuth(app);

export default auth;