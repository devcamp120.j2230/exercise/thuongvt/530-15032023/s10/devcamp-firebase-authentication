import logo from './logo.svg';
import './App.css';


import auth from './fireBase';


import { signInWithPopup, GoogleAuthProvider, signOut,onAuthStateChanged } from "firebase/auth";
import { useEffect, useState } from 'react';

const provider = new GoogleAuthProvider();

function App() {


  const [user, setUser] = useState(null)


  const loginGoogle = () => {
    signInWithPopup(auth, provider)
      .then((result) => {
        console.log(result)
        setUser(result.user)
      })
      .catch((error) => {
        console.log(error)
        setUser(null)
      })
  }

  const logoutGoogle = () => {
    signOut(auth)
      .then(() => {
        console.log("thành công logOut")
        setUser(null)
      })
      .catch((err) => {
        console.log(err)
        setUser(null)
      })
  }


  useEffect(()=>{
    onAuthStateChanged(auth,(result)=>{
      if(result){
        console.log(result)
        setUser(result)
      }
      else{
        console.log(result)
        setUser(null)
      }
    })
  },[])
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        {
          user ?
            <>
              <img src={user.photoURL}></img>
              <p>hello {user.displayName}</p>
              <button onClick={logoutGoogle}>logoutGoogle</button>
            </> :
            <> <p>
              Please singin
            </p>
              <button onClick={loginGoogle}>Singin with google</button></>
        }
      </header>
    </div>
  );
}

export default App;
